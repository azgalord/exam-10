import React, {Component} from 'react';
import Container from "../../components/UI/Container/Container";
import {addNewComment, deleteComment, fetchCommentsData, fetchNewsData} from "../../store/newsActions";
import {connect} from "react-redux";
import {apiURL, noImageUrl} from "../../constants";
import './NewsPage.css';
import Comments from "../../components/Comments/Comments";
import AddComment from "../../components/Comments/AddComment/AddComment";

class NewsPage extends Component {
  state = {
    author: '',
    comment: '',
  };

  componentDidMount() {
    const newsId = this.props.match.params.news_id;
    this.props.fetchNewsData(newsId);
    this.props.fetchCommentsData(newsId);
  }

  getCorrectValueOfImgUrl = (image) => {
    if (!image) return noImageUrl;

    const firstFourChars = image[0] + image[1] + image[2] + image[3];
    return firstFourChars !== 'http' ? apiURL + '/uploads/' + image : image;
  };

  changeHandler(event) {
    this.setState({[event.target.name] : event.target.value});
  }

  addComment = (event) =>  {
    event.preventDefault();
    const body = {...this.state};
    body.news_id = this.props.newsPage.id;
    if (body.comment.length) {
      this.props.addNewComment(body);
      this.setState({author: '', comment: ''});
    }
  };

  onDeleteCommentClick = (event, comment) => {
    const access = window.confirm('Are you sure to delete this comment?');
    event.preventDefault();
    if (access) this.props.deleteComment(comment);
  };

  render() {
    if (!this.props.newsPage) {
      return <div><Container>Loading...</Container></div>
    }
    return (
      <div className="NewsPage">
        <Container>
          <h2 style={{textAlign: 'center'}}>{this.props.newsPage.title}</h2>
          <div className="NewsPageImageContainer">
            <img src={this.getCorrectValueOfImgUrl(this.props.newsPage.image)} alt=""/>
          </div>
          <span>Date: {this.props.newsPage.date}</span>
          <p>{this.props.newsPage.description ? this.props.newsPage.description : 'No description for this news...'}</p>

          <Comments deleteComment={this.onDeleteCommentClick} comments={this.props.comments}/>
          <AddComment
            author={this.state.author}
            comment={this.state.comment}
            onChange={event => this.changeHandler(event)}
            addComment={event => this.addComment(event)}
          />
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  newsPage: state.newsPage,
  comments: state.newsPageComments,
});

const mapDispatchToProps = dispatch => ({
  fetchNewsData: newsId => dispatch(fetchNewsData(newsId)),
  fetchCommentsData: newsId => dispatch(fetchCommentsData(newsId)),
  addNewComment: comment => dispatch(addNewComment(comment)),
  deleteComment: comment => dispatch(deleteComment(comment)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsPage);