import React, {Component, Fragment} from 'react';
import './App.css';
import {AppBar, Button, IconButton, Toolbar, Typography} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import {NavLink, Route, Switch} from "react-router-dom";
import Posts from "../../components/Posts/Posts";
import {connect} from "react-redux";
import {deletePost, fetchNewsData} from "../../store/newsActions";
import AddNewPost from "../AddNewPost/AddNewPost";
import NewsPage from "../NewsPage/NewsPage";

class App extends Component {

  componentDidMount() {
    this.props.fetchNewsData();
  }

  onDeleteClicked = (event, id) => {
    const deleteAccepted = window.confirm('Not this publication will be deleted, a you sure for this action?');
    if (deleteAccepted) this.props.deletePost(id);
  };

  render() {
    return (
      <Fragment>
        <div className="App">
          <AppBar position="static" style={{background: '#2196f3'}}>
            <Toolbar>
              <IconButton color="inherit" aria-label="Menu">
                <MenuIcon/>
              </IconButton>
              <Typography style={{margin: '0 20px'}} variant="h6" color="inherit">
                <NavLink to="/" style={{color: 'inherit', textDecoration: 'none'}}>
                  News
                </NavLink>
              </Typography>
              <Button color="inherit" style={{marginLeft: 'auto', color: 'white'}}>
                <NavLink to="/add" style={{color: 'inherit', textDecoration: 'none'}}>
                  Add new post
                </NavLink>
              </Button>
            </Toolbar>
          </AppBar>
          <Switch>
            <Route path="/" exact component={() =>
              <Posts
                onDeleteClicked={this.onDeleteClicked}
                news={this.props.news}
              />
            }/>
            <Route path="/add" exact component={AddNewPost}/>
            <Route path="/news/:news_id" exact component={NewsPage}/>
          </Switch>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  news: state.news,
});

const mapDispatchToProps = dispatch => ({
  fetchNewsData: newsId => dispatch(fetchNewsData(newsId)),
  deletePost: id => dispatch(deletePost(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
