import React, {Component} from 'react';
import Container from "../../components/UI/Container/Container";
import './AddNewPost.css';
import {Button, TextField} from "@material-ui/core";
import axios from '../../axios-news';
import {connect} from "react-redux";
import {fetchNewsData} from "../../store/newsActions";

class AddNewPost extends Component {
  state = {
    title: '',
    description: '',
    image: '',
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name] : event.target.value});
  };

  fileChangeHandler = event => {
    this.setState({[event.target.name] : event.target.files[0]});
  };

  sendForm = event => {
    event.preventDefault();
    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.setState({title: '', description: '', image: ''});
    axios.post('/news', formData).then(() => {
      this.props.fetchNewsData();
      this.props.history.replace('/');
    });
  };

  render() {
    return (
      <div className="AddNewPost">
        <Container>
          <h2>Add new post</h2>
          <form>
            <TextField
              label="Title"
              name="title"
              value={this.state.title}
              onChange={this.inputChangeHandler}
              margin="normal"
              fullWidth
            />
            <TextField
              label="Content"
              name="description"
              value={this.state.description}
              multiline
              rows="10"
              margin="normal"
              onChange={this.inputChangeHandler}
              fullWidth
              style={{marginBottom: '30px'}}
            />
            <label style={{marginRight: '20px'}} htmlFor="file">Image</label>
            <input
              name="image"
              onChange={this.fileChangeHandler}
              id="file" type="file"/>
            <Button variant="contained"
                    onClick={this.sendForm}
                    color="primary"
                    style={{display: 'block', marginTop: '20px', color: 'white', width: '150px'}}>
              Save
            </Button>
          </form>
        </Container>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchNewsData: newsId => dispatch(fetchNewsData(newsId)),
});

export default connect(null, mapDispatchToProps)(AddNewPost);