import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App/App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from "react-router-dom";

import {Provider} from 'react-redux';
import {applyMiddleware, compose, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';

import newsReducer from './store/newsReducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  newsReducer, composeEnhancers(applyMiddleware(thunkMiddleware))
);

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
serviceWorker.unregister();
