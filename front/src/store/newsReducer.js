import {GET_COMMENTS_RESPONSE, GET_NEWS_PAGE_PESPONSE, GET_NEWS_RESPONSE} from "./newsActions";

const initialState = {
  news: null,
  newsPage: null,
  newsPageComments: null,
};

const newsReducer = (state = initialState, action) => {
  switch (action.type) {
    case (GET_NEWS_RESPONSE):
      return {...state, news: action.response};
    case (GET_NEWS_PAGE_PESPONSE):
      return {...state, newsPage: action.response};
    case (GET_COMMENTS_RESPONSE):
      return {...state, newsPageComments: action.response};
    default:
      return state;
  }
};

export default newsReducer;