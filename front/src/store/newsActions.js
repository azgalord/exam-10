import axios from '../axios-news';

export const GET_NEWS_RESPONSE = 'GET_NEWS_RESPONSE';
export const GET_NEWS_PAGE_PESPONSE = 'GET_NEWS_PAGE_PESPONSE';
export const GET_COMMENTS_RESPONSE = 'GET_COMMENTS_RESPONSE';

export const getNewsResponse = response => ({type: GET_NEWS_RESPONSE, response});
export const getNewsPageResponse = response => ({type: GET_NEWS_PAGE_PESPONSE, response});
export const getCommentsResponse = response => ({type: GET_COMMENTS_RESPONSE, response});

export const fetchNewsData = newsId => {
  return dispatch => {
    axios.get(`/news/${newsId ? newsId : ''}`).then(
      response => {
        if (!newsId) dispatch(getNewsResponse(response.data));
        else dispatch(getNewsPageResponse(response.data));
      },
      error => {
        console.log(error);
      }
    );
  }
};

export const deletePost = postId => {
  return dispatch => {
    axios.delete(`/news/${postId}/`).then(() => {
      dispatch(fetchNewsData())
    });
  }
};

export const fetchCommentsData = newsId => {
  return dispatch => {
    axios.get(`/comments?news_id=${newsId}`).then(
      response => {
        console.log('Comments data success: ', response.data);
        dispatch(getCommentsResponse(response.data));
      }, error => {
        console.log('Comments data error: ', error);
        dispatch(getCommentsResponse(null));
      });
  }
};

export const addNewComment = comment => {
  return dispatch => {
    axios.post('/comments/', comment).then(
      success => {
        console.log('Add comments success: ', success.data);
        dispatch(fetchCommentsData(comment.news_id));
      },
      error => {
        console.log('Add comments error: ', error.data);
      }
    )
  }
};

export const deleteComment = comment => {
  return dispatch => {
    axios.delete(`/comments/${comment.id}`).then(
      success => {
        console.log('Delete comments success: ', success.data);
        dispatch(fetchCommentsData(comment.news_id));
      },error => {
        console.log('Delete comments error: ', error.data);
      }
    )
  }
}