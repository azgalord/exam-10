import React from 'react';
import {Button, Card, CardContent} from "@material-ui/core";
import './NewsCard.css';
import {apiURL, noImageUrl} from "../../constants";
import {Link} from "react-router-dom";

const NewsCard = ({news, onDeleteClicked}) => {
  if (!news.image) news.image = noImageUrl;
  let image = news.image;
  image = image[0] + image[1] + image[2] + image[3];
  return (
    <Card className="NewsCard">
      <Link to={`/news/${news.id}`}/>
      <img src={image !== 'http' ? apiURL + '/uploads/' + news.image : news.image} alt=""/>
      <CardContent>
        <h3>{news.title}</h3>
        <div className="CardContentInner">
          <span>{news.date}</span>
          <Button className="DeleteBtn" variant="contained" color="primary" onClick={onDeleteClicked}>
            Delete
          </Button>
        </div>
      </CardContent>
    </Card>
  );
};

export default NewsCard;