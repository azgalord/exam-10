import React from 'react';
import './CommentItem.css';
import {Button} from "@material-ui/core";

const CommentItem = ({author, comment, deleteComment}) => {
  return (
    <div className="CommentItem">
      <span>{author} wrote: </span>
      <p>{comment}</p>
      <Button variant="contained"
              onClick={deleteComment}
              color="secondary"
              className="DeleteBtn"
              style={{display: 'block', marginTop: '20px', color: 'white', width: '150px'}}>
        Delete
      </Button>
    </div>
  );
};

export default CommentItem;