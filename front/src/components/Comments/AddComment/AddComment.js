import React from 'react';
import './AddComment.css';
import {Button, TextField} from "@material-ui/core";

const AddComment = ({author, comment, onChange, addComment}) => {
  return (
    <div className="AddComment">
      <h3>Add comment</h3>
      <div className="AddCommentInputContainer">
        <TextField
          value={author} onChange={onChange}
          label="Name" name="author"
          margin="normal" fullWidth/>
        <TextField
          required
          multiline rows="10"
          value={comment} onChange={onChange}
          label="Comment" name="comment"
          margin="normal" fullWidth/>
        <Button variant="contained"
                onClick={addComment}
                color="primary"
                style={{display: 'block', marginTop: '20px', color: 'white', width: '150px'}}>
          Add
        </Button>
      </div>
    </div>
  );
};

export default AddComment;