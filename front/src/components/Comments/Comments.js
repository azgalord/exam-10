import React from 'react';
import CommentItem from "./CommentItem/CommentItem";

const Comments = ({comments, deleteComment}) => {
  return (
    <div className="Comments">
      <h3>Comments</h3>
      {comments ? comments.map(comment => (
        <CommentItem
          key={comment.id}
          author={comment.author}
          comment={comment.comment}
          deleteComment={event => deleteComment(event, comment)}
        />
      )) : 'No comments here'}
    </div>
  );
};

export default Comments;