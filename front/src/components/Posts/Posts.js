import React from 'react';
import './Posts.css';
import NewsCard from "../NewsCard/NewsCard";
import Container from "../UI/Container/Container";

const Posts = ({news, onDeleteClicked}) => (
  <div className="Posts">
    <Container>
      <h2>Posts</h2>
      <div className="PostsContainer">
        {news ? news.map((newsItem) => (
          <NewsCard
            key={newsItem.id}
            news={newsItem}
            onDeleteClicked={event => onDeleteClicked(event, newsItem.id)}
          />
        )) : null}
      </div>
    </Container>
  </div>
);

export default Posts;