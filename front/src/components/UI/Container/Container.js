import React from 'react';

const Container = ({children}) => {
  return (
    <div style={{padding: '0 30px'}} className="Container">
      {children}
    </div>
  );
};

export default Container;