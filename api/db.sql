CREATE SCHEMA `news-blog` DEFAULT CHARACTER SET utf8 ;

USE `news-blog`;

CREATE TABLE `news` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `image` VARCHAR(255) NULL,
  `date` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `news_id` INT NOT NULL,
  `author` VARCHAR(255) NULL,
  `comment` TEXT NOT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `comments`
ADD INDEX `news_id_fk_idx` (`news_id` ASC);

ALTER TABLE `comments`
ADD CONSTRAINT `news_id_fk`
  FOREIGN KEY (`news_id`)
  REFERENCES `news-blog`.`news` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

INSERT INTO `news` (`title`, `description`, `image`, `date`)
VALUES ('Два митинга против добычи урана проходят в разных местах в Бишкеке.',
'Первый митинг — на площади Ала-Тоо — проводит "Партия зеленых". Второй жители Иссык-Кульской области проводят в столичном сквере имени Максима Горького. Требования — запретить работы на урановых месторождениях как в Иссык-Кульской области, так и по всей стране.',
'https://sputnik.kg/images//104416/56/1044165672.jpg', '30.04.2019'),
('Россия готовит базу в КР к сдерживанию боевиков из Афганистана — Шойгу',
'БИШКЕК, 29 апр — Sputnik. Министерство обороны России повышает боеготовность своих баз в Таджикистане и Кыргызстане, чтобы предотвратить проникновение боевиков из Афганистана. Об этом, как сообщает РИА Новости, заявил министр обороны РФ Сергей Шойгу сегодня, выступая в Бишкеке на совещании глав оборонных ведомств стран Шанхайской организации сотрудничества.',
'https://sputnik.kg/images//104416/27/1044162717.jpg', '30.04.2019')
;

INSERT INTO `comments` (`news_id`, `author`, `comment`)
VALUES ('1', 'Anonimous', 'Вот и правильно! Нечего им Уран на нашем озере добывать! В СССР такого не было!'),
('2', 'Максим Золоторёв', 'Безопасность это хорошо! Лайк!');