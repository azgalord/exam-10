const express = require('express');

const createRouter = connection => {
  const router = express.Router();

  router.get('/', (req, res) => {
    if (!req.query.news_id) {
      connection.query('SELECT * FROM `comments`', (err, results) => {
        if (err) res.status(500).send({error: 'Database error!'});

        res.send({results});
      });
    } else {
      connection.query('SELECT * FROM `comments` WHERE `news_id` = ?',
        req.query.news_id,
        (err, results) => {
          if (err) {
            res.status(500).send({error: 'Database error!'});
          }

          if (!results.length) {
            res.status(404).send({error: 'Comment not found!'});
          } else {
            res.send(results);
          }
        }
      );
    }
  });

  router.post('/', (req, res) => {
    const comment = req.body;

    if (!comment.author) {
      comment.author = 'Anonymous';
    }

    connection.query('SELECT * FROM `news` WHERE `id` = ?',
      comment.news_id,
      (err, results) => {
        if (err) res.status(500).send({error: 'Database error!'});
        if (results[0]) {
          connection.query('INSERT INTO `comments` (`news_id`, `author`, `comment`) VALUES (?, ?, ?)',
            [comment.news_id, comment.author, comment.comment],
            (err, results) => {
              if (err) res.status(500).send({error: 'Database error!'});
              res.send({message: 'OK!'});
            }
          );
        } else {
          res.status(500).send({message: 'No news with current id found'});
        }
      }
    );
  });

  router.delete('/:id', (req, res) => {
    connection.query('DELETE FROM `comments` WHERE `id` = ?', req.params.id, (err, results) => {
      if (err) res.status(500).send({error: 'Database Error!'});

      res.send({message: 'OK DELETED!'});
    });
  });

  return router;
};

module.exports = createRouter;