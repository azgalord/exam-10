const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const getCorrectDate = date => {
  if (date.toString().length === 1) {
    return '0' + date;
  }
  return date;
};

const createRouter = connection => {
  const router = express.Router();

  router.get('/', (req, res) => {
    connection.query('SELECT * FROM `news`', (err, results) => {
      if (err) res.status(500).send({error: 'Database error!'});

      res.send(results)
    });
  });

  router.get('/:id', (req, res) => {
    connection.query('SELECT * FROM `news` WHERE `id` = ?',
      req.params.id,
      (err, results) => {
        if (err) res.status(500).send({error: 'Database error!'});

        if (!results[0]) {
          res.status(404).send({error: 'News not found'});
          return;
        }
        res.send(results[0]);
      }
    );
  });

  router.post('/', upload.single('image'), (req, res) => {
    const newsItem = req.body;
    const date = new Date();

    const day = getCorrectDate(date.getDate());
    const month = getCorrectDate(date.getMonth());
    const year = getCorrectDate(date.getFullYear());
    newsItem.date = day + '.' + month + '.' + year;

    if (req.file) {
      newsItem.image = req.file.filename;
    }

    connection.query('INSERT INTO `news` (`title`, `description`, `image`, `date`) VALUES (?, ?, ?, ?)',
      [newsItem.title, newsItem.description, newsItem.image, newsItem.date],
      (err, results) => {
        if (err) res.status(500).send('Database error!');
        else {
          res.send({message: 'OK'});
        }
      }
    );
  });

  router.delete('/:id', (req, res) => {
    connection.query('DELETE FROM `news` WHERE `id` = ?',
      req.params.id,
      (err, results) => {
        if (err) res.status(500).send({error: 'Database error!'});
        connection.query('DELETE FROM `comments` WHERE `news_id` = ?', req.params.id,
          (err, results) => {
            if (err) res.status(500).send({error: 'Database error!'});
          }
        );

        res.send({message: 'OK, DELETED!'});
      }
    );
  });

  return router;
};

module.exports = createRouter;